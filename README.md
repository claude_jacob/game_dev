*************************************
Created by: Claude Jacob Aparecio
*************************************
RUNNING THE GAME:

npm install

node server.js

go to localhost:8081

*************************************

GAME INSTRUCTIONS/STORY:

A town is attacked by raiders, it is your job to keep the town safe.

In order to kill the raiders, you have to click them. If you can't kill the

raiders the town's live will decrease. 

DON'T LET THE TOWN CONQUERED BY RAIDERS!

*************************************

BONUS:

You have a purge button located in the upper left corner of the screen.

When you kill a raider your purge poll will increase, you need to kill 15 raiders

in order to use the "Purge" button.

When the purge button is clicked, all the raiders will die but would not credit to your kills.

Good luck & Have fun!