var config = {
  type: Phaser.AUTO,
  parent: 'phaser-example',
  width: 600,
  height: 900,
  physics: {
    default: 'arcade',
    arcade: {
      debug: false,
      gravity: { y: 50 }
    }
  },
  scene: {
    preload: preload,
    create: create,
  } 
};

window.onload = () => {
  document.getElementById('purgeButton').disabled = true
  document.getElementById('gameover-container').hidden = true
}
 
const game = new Phaser.Game(config);
var player
function preload() {
  this.load.image('background', 'assets/bg.png');
  this.load.image('raider', 'assets/raider.png');
  this.load.image('town', 'assets/town.png');
  this.load.image('building', 'assets/middle.png');
}

let platforms
let interval = 5;
let timedEvent
let currentTime = 0
let playing = true
let raiders = []
let buildings
let purgePoll = 0
let kills = 0
let life = 21
function create() {
  this.add.image(100,360,'background');
  this.add.image(280,780,'town');
  platforms = this.physics.add.staticGroup()
  buildings = this.physics.add.group({
    key: 'building',
    repeat: 15,
    setXY: { x: 12, y: 0, stepX: 38 }
  })
  platforms.create(80, 820, 'building').setScale(2).refreshBody()
  platforms.create(10, 850, 'building').setScale(2).refreshBody()
  platforms.create(160, 900, 'building').setScale(2).refreshBody()
  platforms.create(220, 860, 'building').setScale(2).refreshBody()
  platforms.create(300, 770, 'building').setScale(2).refreshBody()
  platforms.create(350, 800, 'building').setScale(2).refreshBody()
  platforms.create(450, 750, 'building').setScale(2).refreshBody()
  platforms.create(550, 800, 'building').setScale(2).refreshBody()

  this.physics.add.collider(buildings, platforms);

  timedEvent = this.time.addEvent({
    delay: 500,
    callback: listener,
    callbackScope: this,
    loop: true
  });
}

function listener() {
  currentTime = 999999999999 - timedEvent.repeatCount
  if (currentTime%interval === 0) {
    let raider = this.physics.add.sprite(Math.random() * 800, 0, 'raider')
    raider.setCollideWorldBounds(true);
    raider.setInteractive()
    raider.inputEnabled = true
    raider.on('pointerdown', () => {
    raider.destroy()
        
        if (purgePoll > 15) {
          document.getElementById('purgeButton').disabled = false
          document.getElementById('purgeButton').style.backgroundColor = 'blue'
        }
        
        kills++
        document.getElementById('kills').innerHTML = `Kills: ${kills}`
        purgePoll++

        if (purgePoll <= 15) {
          let levelPoll = ''
          for(let x = 0 ;x < purgePoll; x++) {
            levelPoll+= '|'
          }
          document.getElementById('purgePoll').innerHTML = `Purge Poll: [${levelPoll}]`
        }
      })
      if (currentTime%5 === 0 && interval > 1) {
        interval--
      }
      raiders.push(raider)
    this.physics.add.collider(raider, platforms);
    this.physics.add.overlap(raider, buildings, takeDamage, null, this);
  }
}

function takeDamage(raider, platform) {
  raider.disableBody(true, true)
  if (life > 0) {
    life--
  } else {
    document.getElementById('gameover-container').hidden = false
  }
  document.getElementById('life').innerHTML = `${life} LIVES LEFT`
}

function purge(id) {
  for(let raider of raiders) {
    raider.destroy()
  }
  purgePoll = 0
  document.getElementById('purgePoll').innerHTML = `Purge Poll: []`
  document.getElementById('purgeButton').disabled = true
  document.getElementById('purgeButton').style.backgroundColor = 'white'
}

function restart() {
  location.reload()
  document.getElementById('gameover-container').hidden = true
}